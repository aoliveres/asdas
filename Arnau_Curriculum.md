Arnau Oliveres Alias

![](/images/arnau.png)
***
> Telefono de contacto: +34 626593948

> Correo electronico: aoliveres@gmail.com
***

## **Sobre Mi:**  ##

_Soy una persona que me gusta aprender y trabajar, soy sociable y rápidamente me adapto al trabajo. Muy flexible a la hora de realizar cualquier tarea en equipo o no. Me considero una persona organizada y puntual._

## **Competencias laborales** ##

- Bueno Conocimiento de Libre Office y Microsoft Office
- Conocimientos al instalar Sistemas Operativos con ***Linux*** y ***Windows***
- Capacidad de montar y desmontar un ordenador
- Capacidad escrita y leída del Inglés.

## **Formación** ##

2017 - presente  --  Actualmente estudiando Grado Medio de Sistemas Microinformaticos y Redes en el Instituto Poblenou (ECAIB) de Barcelona

## **Competencias Personales** ##

1. Trabajador con ganas de aprender
2. Comunicativo y trabajador en equipo
3. Capacidad de realizar diferentes trabajos

| **Castellano** | **Ingles** |
| ---------- | ---------- |
| Nivel materno   | Lectura y escriptura   |

